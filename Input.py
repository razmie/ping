import Game
import Common

# Handles player input of the game.
class Input:

    # References the gane that created this input.
    game = None

    def __init__(self, game):
        self.game = game

        # Setup input bindings.
        self.game.screen.listen()

        # Listen to escape key being pressed so that we can quit the game.
        self.game.screen.onkey(self.quit_game, "Escape")

        # Listen to the window's close button being pressed.
        self.game.root.protocol("WM_DELETE_WINDOW", self.quit_game)

        self.game.screen.onkeypress(self.frontend_key_press0, "1")
        self.game.screen.onkeypress(self.frontend_key_press1, "2")
        self.game.screen.onkeypress(self.frontend_key_press2, "3")
        self.game.screen.onkeypress(self.frontend_key_press3, "4")

        # Listen to key presses to move paddles.

        self.game.screen.onkeypress(self.paddle0_up_pressed, "Q")
        self.game.screen.onkeypress(self.paddle0_up_pressed, "q")
        self.game.screen.onkeyrelease(self.paddle0_up_released, "Q")
        self.game.screen.onkeyrelease(self.paddle0_up_released, "q")
        self.game.screen.onkeypress(self.paddle0_down_pressed, "A")
        self.game.screen.onkeypress(self.paddle0_down_pressed, "a")
        self.game.screen.onkeyrelease(self.paddle0_down_released, "A")
        self.game.screen.onkeyrelease(self.paddle0_down_released, "a")

        self.game.screen.onkeypress(self.paddle1_up_pressed, "P")
        self.game.screen.onkeypress(self.paddle1_up_pressed, "p")
        self.game.screen.onkeyrelease(self.paddle1_up_released, "P")
        self.game.screen.onkeyrelease(self.paddle1_up_released, "p")
        self.game.screen.onkeypress(self.paddle1_down_pressed, "L")
        self.game.screen.onkeypress(self.paddle1_down_pressed, "l")
        self.game.screen.onkeyrelease(self.paddle1_down_released, "L")
        self.game.screen.onkeyrelease(self.paddle1_down_released, "l")

        self.game.screen.onkeypress(self.paddle2_up_pressed, "H")
        self.game.screen.onkeypress(self.paddle2_up_pressed, "h")
        self.game.screen.onkeyrelease(self.paddle2_up_released, "H")
        self.game.screen.onkeyrelease(self.paddle2_up_released, "h")
        self.game.screen.onkeypress(self.paddle2_down_pressed, "N")
        self.game.screen.onkeypress(self.paddle2_down_pressed, "n")
        self.game.screen.onkeyrelease(self.paddle2_down_released, "N")
        self.game.screen.onkeyrelease(self.paddle2_down_released, "n")

        self.game.screen.onkeypress(self.paddle3_up_pressed, "Up")
        self.game.screen.onkeyrelease(self.paddle3_up_released, "Up")
        self.game.screen.onkeypress(self.paddle3_down_pressed, "Down")
        self.game.screen.onkeyrelease(self.paddle3_down_released, "Down")

    # Sets loopGame to false to quit the game.
    def quit_game(self):
        self.game.loopGame = False

    def frontend_key_press0(self):
        self.game.gameMode = Common.GameModes.OneVsAI

    def frontend_key_press1(self):
        self.game.gameMode = Common.GameModes.OneVsOne

    def frontend_key_press2(self):
        self.game.gameMode = Common.GameModes.TwoVsTwo

    def frontend_key_press3(self):
        self.game.gameMode = Common.GameModes.AIVsAI

    # P1 keys.

    def paddle0_up_pressed(self):
        self.game.paddles[0].moveUp = True

    def paddle0_up_released(self):
        self.game.paddles[0].moveUp = False

    def paddle0_down_pressed(self):
        self.game.paddles[0].moveDown = True

    def paddle0_down_released(self):
        self.game.paddles[0].moveDown = False

    # P2 keys

    def paddle1_up_pressed(self):
        self.game.paddles[1].moveUp = True

    def paddle1_up_released(self):
        self.game.paddles[1].moveUp = False

    def paddle1_down_pressed(self):
        self.game.paddles[1].moveDown = True

    def paddle1_down_released(self):
        self.game.paddles[1].moveDown = False

    # P3 keys

    def paddle2_up_pressed(self):
        self.game.paddles[2].moveUp = True

    def paddle2_up_released(self):
        self.game.paddles[2].moveUp = False

    def paddle2_down_pressed(self):
        self.game.paddles[2].moveDown = True

    def paddle2_down_released(self):
        self.game.paddles[2].moveDown = False

    # P4 keys

    def paddle3_up_pressed(self):
        self.game.paddles[3].moveUp = True

    def paddle3_up_released(self):
        self.game.paddles[3].moveUp = False

    def paddle3_down_pressed(self):
        self.game.paddles[3].moveDown = True

    def paddle3_down_released(self):
        self.game.paddles[3].moveDown = False
