import Game
import Paddle
import Common
import enum

class AIStates(enum.Enum):
    Decide = 0
    Stay = 1
    MoveUp = 2
    MoveDown = 3

# Handles AI of the game.
class AIController:

    # References the gane that created this input.
    game = None

    # References the paddle this AI is controlling
    paddle = None

    # True if this AI is activated.
    activated = False

    stateTimer = 0.0
    maxDecidedTimer = 1.0

    states = AIStates.Decide

    # Controls how much it wants the ball centered to the paddle.
    heightMargin = 30

    def __init__(self, game, paddle):
        self.game = game
        self.paddle = paddle

    def update(self, deltaTime):
        if not self.activated:
            return

        ball = self.game.ball

        # Wait for time to decide.
        self.stateTimer -= deltaTime
        if self.stateTimer <= 0.0:
            self.stateTimer = 0.0

        if self.states == AIStates.Decide:
              
            # Decied if we want to move up or down based on the height difference between the ball and paddle.
            ballY = ball.pen.ycor()
            paddleY = self.paddle.pen.ycor()

            if ballY > paddleY + self.paddle.halfHeight * self.game.get_paddle_size_factor() - self.heightMargin:
                self.states = AIStates.MoveUp
                self.stateTimer = 0.1
                
            elif ballY < paddleY - self.paddle.halfHeight * self.game.get_paddle_size_factor() + self.heightMargin:
                self.states = AIStates.MoveDown
                self.stateTimer = 0.1
            else:
                self.states = AIStates.Stay
                self.stateTimer = 0.1

        elif self.states == AIStates.MoveUp:

            self.paddle.moveUp = True

            if self.stateTimer == 0.0:
                self.paddle.moveUp = False
                self.states = AIStates.Decide

        elif self.states == AIStates.MoveDown:

            self.paddle.moveDown = True

            if self.stateTimer == 0.0:
                self.paddle.moveDown = False
                self.states = AIStates.Decide

        elif self.states == AIStates.Stay:

            self.paddle.moveUp = False
            self.paddle.moveDown = False

            if self.stateTimer == 0.0:
                self.states = AIStates.Decide

