import turtle
import os
import time
from Paddle import Paddle
from Ball import Ball
from Common import GameStates
from Common import GameModes
import Input
from AI import AIController

class Game:
    # The state of the running game.
    gameState = GameStates.Frontend
    gameStateDelay = 0.0

    # What kind of mode we're playing. 2 players, 4 players, etc.
    gameMode = GameModes.Unselected

    # How many frames per second should we update the game.
    FPS = 60

    # Controls the main game loop. The game will continously loop until this is false.
    loopGame = True

    # Gameplay. How many scores needed to win the game.
    endGameThreshold = 5

    # Set up variables needed to calculate delta time.
    lastFrameTime = 0
    lastFrameTime = time.time()

    # Screen area sizes.
    screenWidth = 1200
    screenHeight = 700

    # UI. The heigh of the top area where the scores are.
    topHeight = 50

    # UI. Used for scoring.
    scorePens = []
    scores = [0, 0]

    # UI. Displays the name of the game in the frontend.
    gameTitle = None

    # UI. The text in frontend describing the different game modes and keys.
    frontendText = None

    # UI. Text used when a players have won.
    winText = None

    blinkTimer = 0.0
    blinker = None

    # Ball position in the frontend.
    ballStartX = -53
    ballStartY = 171.5

    # The factor used to shrink the paddle in a four player game.
    # 0.75 means it's 75% of the original size.
    paddleSmallFactor = 0.75

    root = None
    screen = None
    input = None

    # objects used in the game.
    ball = None
    AIs = []

    def __init__(self):

        # Create window.
        self.screen = turtle.Screen()
        self.screen.setup(self.screenWidth, self.screenHeight)
        self.screen.bgcolor("black")
        self.screen.title("PiNG")
        self.screen.tracer(0, 0)

        canvas = self.screen.getcanvas()
        self.root = canvas.winfo_toplevel()

        # Stop window from being resized.
        self.root.resizable(False, False)

        # Create input.
        self.input = Input.Input(self)

        # Draw top border below the scores.
        borderPen = turtle.Turtle()
        borderPen.color("white")
        borderPen.speed(0)
        borderPen.pensize(3)
        borderPen.penup()
        borderPen.setposition(-self.screenWidth/2,
                              self.screenHeight/2-self.topHeight)
        borderPen.pendown()
        borderPen.forward(self.screenWidth)
        borderPen.left(90)
        borderPen.hideturtle()

        # Used for blinking when a goal is scored.
        self.blinker = turtle.Turtle()
        self.show_blinker(False)

        # Create score texts for left and right.
        self.scorePens.append(turtle.Turtle())
        self.scorePens.append(turtle.Turtle())

        self.scorePens[0].color("white")
        self.scorePens[0].penup()
        self.scorePens[0].setposition(-500, 300)
        self.scorePens[0].pendown()
        self.scorePens[0].hideturtle()
        self.scorePens[0].write("0", False, "center",
                                font=("Courier", 30, "normal"))
        self.scorePens[0].hideturtle()

        self.scorePens[1].color("white")
        self.scorePens[1].penup()
        self.scorePens[1].setposition(500, 300)
        self.scorePens[1].pendown()
        self.scorePens[1].hideturtle()
        self.scorePens[1].write("0", False, "center",
                                font=("Courier", 30, "normal"))
        self.scorePens[1].hideturtle()

        # Create the game title
        self.gameTitle = turtle.Turtle()

        # Create the frontend text.
        self.frontendText = turtle.Turtle()
        self.show_frontend_text(True)

        # Create the wining text.
        self.winText = turtle.Turtle()
        self.winText.hideturtle()

        # Create shapes for paddles.

        # Normal size shapes for white and black.
        paddleShape = turtle.Shape("compound")
        poly = ((-Paddle.halfWidth, -Paddle.halfHeight), (-Paddle.halfWidth, Paddle.halfHeight),
                (Paddle.halfWidth, Paddle.halfHeight), (Paddle.halfWidth, -Paddle.halfHeight))
        paddleShape.addcomponent(poly, "white", "white")
        self.screen.register_shape("paddleShape", paddleShape)

        paddleShape = turtle.Shape("compound")
        poly = ((-Paddle.halfWidth, -Paddle.halfHeight), (-Paddle.halfWidth, Paddle.halfHeight),
                (Paddle.halfWidth, Paddle.halfHeight), (Paddle.halfWidth, -Paddle.halfHeight))
        paddleShape.addcomponent(poly, "black", "black")
        self.screen.register_shape("paddleShapeBlack", paddleShape)

        # Smalled size shapes for white and black. This is used in 4 players.
        paddleShape = turtle.Shape("compound")
        poly = ((-Paddle.halfWidth, -Paddle.halfHeight * self.paddleSmallFactor), (-Paddle.halfWidth, Paddle.halfHeight * self.paddleSmallFactor),
                (Paddle.halfWidth, Paddle.halfHeight * self.paddleSmallFactor), (Paddle.halfWidth, -Paddle.halfHeight * self.paddleSmallFactor))
        paddleShape.addcomponent(poly, "white", "white")
        self.screen.register_shape("paddleShapeSmall", paddleShape)

        paddleShape = turtle.Shape("compound")
        poly = ((-Paddle.halfWidth, -Paddle.halfHeight * self.paddleSmallFactor), (-Paddle.halfWidth, Paddle.halfHeight * self.paddleSmallFactor),
                (Paddle.halfWidth, Paddle.halfHeight * self.paddleSmallFactor), (Paddle.halfWidth, -Paddle.halfHeight * self.paddleSmallFactor))
        paddleShape.addcomponent(poly, "black", "black")
        self.screen.register_shape("paddleShapeSmallBlack", paddleShape)

        # Create the ball object.
        self.ball = Ball(self.ballStartX, self.ballStartY, -self.screenHeight/2, self.screenHeight /
                         2 - self.topHeight, self.ball_goal)

        # Create all paddle objects.
        self.paddles = []
        self.paddles.append(Paddle(-500, 0, -self.screenHeight/2,
                                   self.screenHeight/2-self.topHeight))
        self.paddles.append(Paddle(500, 0, -self.screenHeight/2,
                                   self.screenHeight / 2 - self.topHeight))

        # Move paddle 3 and 4 outside the screen. When 4 player game mode is chosen, they will be moved in.
        self.paddles.append(Paddle(-1000, 0, -self.screenHeight/2,
                                   self.screenHeight/2-self.topHeight))
        self.paddles.append(Paddle(1000, 0, -self.screenHeight/2,
                                   self.screenHeight / 2 - self.topHeight))

        # Create AIs
        self.AIs.append(AIController(self, self.paddles[0]))
        self.AIs.append(AIController(self, self.paddles[1]))

        # Main game loop.
        while self.loopGame == True:

            # Calculate delta time.
            self.currentTime = time.time()
            deltaTime = self.currentTime - self.lastFrameTime
            self.lastFrameTime = self.currentTime

            # Countdown generic timer.
            if self.gameStateDelay > 0.0:
                self.gameStateDelay -= deltaTime
                if self.gameStateDelay <= 0.0:
                    self.gameStateDelay = 0.0

            # Process game states.
            if self.gameState == GameStates.Frontend:

                # FRONTEND.
                # Wait until a game mode have beend selected.

                if self.gameMode != GameModes.Unselected:

                    # A mode has been selected. Go to start play with a delay while in that state.
                    self.gameStateDelay = 3.0
                    self.gameState = GameStates.StartPlay

                    # Hide the winning text.
                    self.winText.reset()
                    self.winText.hideturtle()

                    # Hide frontend.
                    self.show_frontend_text(False)

                    if self.gameMode == GameModes.OneVsOne or self.gameMode == GameModes.OneVsAI or self.gameMode == GameModes.AIVsAI:

                        # Move the other 2 paddles outside the screen so they won't be seen.

                        self.paddles[2].create_pen(-1000, 0, self.gameMode,
                                                   False, self.get_paddle_size_factor())
                        self.paddles[3].create_pen(
                            1000, 0, self.gameMode, False, self.get_paddle_size_factor())

                    elif self.gameMode == GameModes.TwoVsTwo:

                        # Move the other 2 paddles back into the playing area.

                        self.paddles[2].create_pen(-550, 0, self.gameMode,
                                                   False, self.get_paddle_size_factor())
                        self.paddles[3].create_pen(
                            550, 0, self.gameMode, False, self.get_paddle_size_factor())

                    # Enable AIs if necessary.
                    if self.gameMode == GameModes.OneVsAI:
                        self.AIs[1].activated = True
                    elif self.gameMode == GameModes.AIVsAI:
                        self.AIs[0].activated = True
                        self.AIs[1].activated = True
                    else:
                        self.AIs[0].activated = False
                        self.AIs[1].activated = False

                    # Setup the paddles. We change the size based on how many are playing. 4 players have smaller paddles.
                    for paddle in self.paddles:
                        x = paddle.pen.xcor()
                        y = paddle.pen.ycor()

                        paddle.create_pen(x, y, self.gameMode, False, self.get_paddle_size_factor())

            elif self.gameState == GameStates.StartPlay:

                # START PLAY
                # Wait for the delay before continuing to the next state.

                if self.gameStateDelay == 0.0:
                    self.gameState = GameStates.Play
                elif self.gameStateDelay <= 2.0:

                    # In the middle of waiting, reset the ball position to the center of the screen.
                    self.reset_ball_position()

            elif self.gameState == GameStates.Play:

                # PLAY

                # Check if ball is hitting paddles.
                for paddle in self.paddles:

                    # When the ball hits a paddle, it momentarily becomes invincible from another paddle hit.
                    # This is because it allows it to bounce clean off the paddle and won't keep colliding with
                    # the same paddle in the following updates.
                    if self.ball.canCollideWithPaddle == True:

                        # Do a collision check between the ball and a paddle to see if it's bieng hit.
                        isHit = self.ball.is_hitting_paddle(
                            paddle.pen.xcor(), paddle.pen.ycor(), Paddle.halfWidth, Paddle.halfHeight * self.get_paddle_size_factor())
                        if isHit:

                            # The ball is hit. Bounce it.
                            self.ball.bounce_x()

                            # Move the direction of the ball as much as the movement of the paddle
                            # to slice the ball.
                            self.ball.dirY += paddle.accY / paddle.moveSpeed * 1.5
                            continue

                # Update ball.
                self.ball.update(deltaTime)

            elif self.gameState == GameStates.EndPlay:

                # END PLAY
                # Wait for the delay and go back to start play. While waiting, tick down blink timer so that it can be turned off.

                self.blinkTimer -= deltaTime
                if self.blinkTimer <= 0.0:
                    self.blinkTimer = 0.0
                    self.show_blinker(False)

                    # Turn paddle white again.
                    for paddle in self.paddles:
                        x = paddle.pen.xcor()
                        y = paddle.pen.ycor()

                        paddle.create_pen(x, y, self.gameMode,
                                          False, self.get_paddle_size_factor())

                if self.gameStateDelay == 0.0:
                    self.gameStateDelay = 2.0
                    self.gameState = GameStates.StartPlay

            elif self.gameState == GameStates.EndGame:

                # END GAME
                # Wait for the the delay and go back to front end.

                if self.gameStateDelay == 0.0:
                    self.gameState = GameStates.Frontend
                    self.gameMode = GameModes.Unselected

                    self.reset_scores()
                    self.show_frontend_text(True)
                    self.reset_ball_position()

                    self.winText.reset()

            # Update AIs.
            for AI in self.AIs:
                AI.update(deltaTime)

            # Update paddles.
            for paddle in self.paddles:
                paddle.update(deltaTime)

            # Need to update the window, otherwise it will hang in this loop.
            self.screen.update()

            sleepTime = 1.0 / self.FPS - \
                (self.currentTime - self.lastFrameTime)
            if sleepTime > 0:
                time.sleep(sleepTime)

    def ball_goal(self, side):

        # Increase score and update text.
        self.scores[side] += 1
        self.scorePens[side].clear()
        self.scorePens[side].write(str(self.scores[side]), False, "center",
                                   font=("Courier", 30, "normal"))

        # Show winning text if reached threshold.
        if self.scores[side] >= self.endGameThreshold:
            self.winText.reset()
            self.winText.color("white")
            self.winText.penup()
            self.winText.setposition(0, 0)
            self.winText.pendown()
            self.winText.hideturtle()

            if self.gameMode == GameModes.OneVsAI:
                if side == 0:
                    text = "P1 Wins!"
                else:
                    text = "AI Wins!"
            elif self.gameMode == GameModes.OneVsOne:
                text = "P" + str(side + 1) + " Wins!"
            elif self.gameMode == GameModes.TwoVsTwo:
                if side == 0:
                    text = "P1 & P3 Wins!"
                else:
                    text = "P2 & P4 Wins!"
            elif self.gameMode == GameModes.AIVsAI:
                text = "AI Wins!"

            self.winText.write(text, False, "center",
                               font=("Courier", 60, "normal"))

            self.gameStateDelay = 4.0
            self.gameState = GameStates.EndGame
        else:
            self.gameStateDelay = 1.0
            self.gameState = GameStates.EndPlay
            self.blinkTimer = 0.2
            self.show_blinker(True)

            # Turn paddle black.
            for paddle in self.paddles:
                x = paddle.pen.xcor()
                y = paddle.pen.ycor()

                paddle.create_pen(x, y, self.gameMode, True,
                                  self.get_paddle_size_factor())

    def show_frontend_text(self, show):
        self.gameTitle.reset()
        self.frontendText.reset()

        if show:
            self.gameTitle.color("white")
            self.gameTitle.penup()
            self.gameTitle.setposition(0, 30)
            self.gameTitle.pendown()
            self.gameTitle.hideturtle()
            self.gameTitle.write("PiNG", False, "center",
                                 font=("Courier", 120, "bold"))

            text = "[1] P1 vs AI\n" + \
                "[2] P1 vs P2\n" + \
                "[3] P1,P3 vs P2,P4\n" + \
                "[4] AI vs AI\n\n\n\n\n\n" + \
                "          Keys:\n" + \
                "P1: Q & A      P2: P & L\n" + \
                "P3: H & N      P4: Up & Down"

            self.frontendText.color("white")
            self.frontendText.penup()
            self.frontendText.setposition(0, -270)
            self.frontendText.pendown()
            self.frontendText.hideturtle()
            self.frontendText.write(text, False, "center",
                                    font=("Courier", 18, "normal"))

        self.gameTitle.hideturtle()
        self.frontendText.hideturtle()

    def show_blinker(self, show):

        self.blinker.reset()

        if show:
            self.blinker.color("#A0A0A0")
            self.blinker.speed(0)
            self.blinker.begin_fill()
            self.blinker.setposition(-self.screenWidth/2, -self.screenHeight/2)
            self.blinker.forward(self.screenWidth)
            self.blinker.left(90)
            self.blinker.forward(self.screenHeight-self.topHeight)
            self.blinker.left(90)
            self.blinker.forward(self.screenWidth)
            self.blinker.left(90)
            self.blinker.forward(self.screenHeight-self.topHeight)
            self.blinker.left(90)
            self.blinker.end_fill()
            self.blinker.hideturtle()

    def reset_scores(self):
        index = 0
        while index < 2:
            self.scores[index] = 0
            self.scorePens[index].clear()
            self.scorePens[index].write(str(self.scores[index]), False, "center",
                                        font=("Courier", 30, "normal"))
            index += 1

    def reset_ball_position(self):
        if self.scores[0] == 0 and self.scores[1] == 0:
            self.ball.reset(self.ballStartX, self.ballStartY)
        else:
              self.ball.reset()

    def get_paddle_size_factor(self):
        if self.gameMode == GameModes.TwoVsTwo:
            return self.paddleSmallFactor

        return 1.0
