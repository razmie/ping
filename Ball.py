import turtle
import math
import random

class Ball:
    pen: turtle.Turtle

    # The direction of the ball.
    dirX = 1.0
    dirY = 1.0

    # The play area. Used for boundary collision.
    areaBottom = 0.0
    areaTop = 0.0

    # The current moving speed.
    moveSpeed = 400.0

    # The minimum moving speed.
    minSpeed = 400.0

    # The maximum moving speed.
    maxSpeed = 1800.0

    speedUpTime = 0.0

    # How long it'll take to reach max moving speed.
    maxSpeedUpTime = 60.0

    # The radius of the ball.
    radius = 14

    # True if the ball can be hit by a paddle.
    canCollideWithPaddle = True

    lastX = 0
    lastY = 0
    canCollidePaddleTimer = 0.0

    def __init__(self, x, y, areaBottom, areaTop, onGoalFun):

        self.pen = turtle.Turtle()
        self.pen.shapesize(self.radius/12, self.radius/12)
        self.pen.shape("circle")
        self.pen.color("white")
        self.pen.penup()
        self.pen.speed(0)
        self.pen.setposition(x, y)
        self.pen.setheading(90)

        self.areaBottom = areaBottom
        self.areaTop = areaTop

        self.onGoalFunc = onGoalFun

    def reset(self, x=0, y=0):

        self.pen.setx(x)
        self.pen.sety(y)
        self.speedUpTime = 0.0
        
        # Randomise the starting direction.
        self.dirX = -1 + random.random() * 2.0
        self.dirY = -1 + random.random() * 2.0

    def update(self, deltaTime):

        x = self.lastX = self.pen.xcor()
        y = self.lastY = self.pen.ycor()

        # Slowly reduce the y direction so that it moves more horizontally over time.
        self.dirY -= self.dirY * deltaTime * 0.5
        
        self.dirX, self.dirY = self.normalise_vector(self.dirX, self.dirY)

        # Increase speed over time. It gets faster and faster.
        self.speedUpTime += deltaTime
        if self.speedUpTime > self.maxSpeedUpTime:
            self.speedUpTime = self.maxSpeedUpTime

        speedUpAlpha = self.speedUpTime / self.maxSpeedUpTime
        self.moveSpeed = self.minSpeed +  (speedUpAlpha * (self.maxSpeed - self.minSpeed))

        x += self.dirX * self.moveSpeed * deltaTime
        y += self.dirY * self.moveSpeed * deltaTime

        # Check if the ball has reached goal positions.
        if x > 600 + self.radius:
            self.onGoalFunc(0)
        elif x < -600 - self.radius:
            self.onGoalFunc(1)

        # Check if the ball has reached the top or bottom.
        if y > self.areaTop - self.radius:
            y = self.areaTop - self.radius
            self.dirY *= -1
        elif y < self.areaBottom + self.radius:
            y = self.areaBottom + self.radius
            self.dirY *= -1

        self.pen.setx(x)
        self.pen.sety(y)

        # Count down until the ball can collide with paddles again.
        if self.canCollidePaddleTimer > 0.0:
            self.canCollidePaddleTimer -= deltaTime
            if self.canCollidePaddleTimer < 0.0:
                self.canCollidePaddleTimer = 0.0
                self.canCollideWithPaddle = True

    def normalise_vector(self, x, y):
        w = math.sqrt(x * x + y * y)
        x /= w
        y /= w
        return x, y

    def is_hitting_paddle(self, paddleX, paddleY, paddleHalfWidth, paddleHalfHeight):
        x = self.pen.xcor()
        y = self.pen.ycor()

        if (x < paddleX + paddleHalfWidth + self.radius) and \
            (x > paddleX - paddleHalfWidth - self.radius) and \
            (y < paddleY + paddleHalfHeight + self.radius) and \
            (y > paddleY - paddleHalfHeight - self.radius):
            return True
        return False

    def bounce_x(self):
        self.dirX *= -1
        self.canCollideWithPaddle = False
        self.canCollidePaddleTimer = 0.25

    def bounce_y(self):
        self.dirY *= -1
        self.canCollideWithPaddle = False
        self.canCollidePaddleTimer = 0.25
        
