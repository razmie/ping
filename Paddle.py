import turtle
from Common import GameModes

class Paddle:
    pen: turtle.Turtle

    # The play area. Used for boundary collision.
    areaBottom = 0.0
    areaTop = 0.0

    # True if the paddle is currently being moved up.
    moveUp = False
    # True if the paddle is currently being moved down.
    moveDown = False

    # The maximum speed of the paddle.
    moveSpeed = 600.0

    # Values used for acceleration and decceleration calculations.
    acceleration = 10.0
    decceleration = 10.0

    # The current Y acceleration.
    accY = 0.0

    # The half sizes of the paddle.
    halfWidth = 7.5
    halfHeight = 75

    # Scaling used to calculate it's height. 1.0 is normal scaling.
    sizeFactor = 1.0

    def __init__(self, x, y, areaBottom, areaTop):

        self.pen = turtle.Turtle()
        self.create_pen(x, y, GameModes.OneVsOne, False, 1.0)

        self.areaBottom = areaBottom
        self.areaTop = areaTop

    def create_pen(self, x, y, gameMode, isBlack, sizeFactor):

        self.pen.reset()
        self.pen.penup()
        self.pen.setposition(x, y)
        self.pen.pendown()

        self.sizeFactor = sizeFactor

        # Use different shapes based on the chosen game mode.
        if gameMode == GameModes.OneVsOne or gameMode == GameModes.OneVsAI or gameMode == GameModes.AIVsAI:
            if isBlack:
                self.pen.shape("paddleShapeBlack")
            else:
                self.pen.shape("paddleShape")
        else:
            if isBlack:
                self.pen.shape("paddleShapeSmallBlack")
            else:
                self.pen.shape("paddleShapeSmall")

        self.pen.penup()
        self.pen.speed(0)
        self.pen.setheading(90)

    def update(self, deltaTime):
        
        y = self.pen.ycor()

        # Caclulate acceleration based on if moving up or down.
        if self.moveUp:
            self.accY += self.moveSpeed * self.acceleration * deltaTime
        elif self.moveDown:
            self.accY -= self.moveSpeed * self.acceleration * deltaTime
        else:
            self.accY -= self.accY * self.decceleration * deltaTime

        # Cap acceleration so it doesn't go faster than the speed.
        if self.accY > self.moveSpeed:
            self.accY = self.moveSpeed
        elif self.accY < -self.moveSpeed:
            self.accY = -self.moveSpeed

        # Apply acceleration to y position.
        y += self.accY * deltaTime

        # Stop from going outside the screen.
        if y > self.areaTop - self.halfHeight * self.sizeFactor:
            y = self.areaTop - self.halfHeight * self.sizeFactor
            self.accY = 0
        elif y < self.areaBottom + self.halfHeight * self.sizeFactor:
            y = self.areaBottom + self.halfHeight * self.sizeFactor
            self.accY = 0

        self.pen.sety(y)
