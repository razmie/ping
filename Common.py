import turtle
import enum

class GameStates(enum.Enum):
    Frontend = 0
    StartPlay = 1
    Play = 2
    EndPlay = 3
    EndGame = 4

class GameModes(enum.Enum):
    Unselected = 0
    OneVsAI = 1         # One player vs AI
    OneVsOne = 2        # One player vs another player.
    TwoVsTwo = 3        # Two players vs two players.
    AIVsAI = 4          # AI vs AI
